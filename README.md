# Logic in Agda to LaTeX

This project was done for fun in order to verify and typeset assignment
solutions for a logic course.
The project consists of an Agda library for solving specific logic problems as
well as a Haskell program to convert annotated Agda code into LaTeX.

This was a quick and dirty project, thus the design of the library and the
organization of the code is very rough.
The included assignmnet solutions serve as examples of how to use the library
and how to annotate the code.

I also introduce a natural deduction for a subset of LTL and CTL.

## The Logic Library

The logic library for Agda consists of 5 parts which are not fully compatible
with each other.
The notation is at times very confusing and is more designed with the Haskell
code in mind rather than the user.
It is not idomatic Agda, and it heavily relies on postulates which breaks the
correctness guarantees of Agda.
The library is designed to verify solutions rather than solve problems.
The only documentation is a few comments and the examples.

## The Agda to LaTeX Converter

The Haskell program creates LaTeX documents from annotated Agda code and then
invokes pdflatex to produce the pdf documents.
The program accepts any number of Agda files as arguments.

## The Annotations

As with the logic library the annotations are only documented through a few
comments and the examples.
The annotations are named after the type of problem it is used to solve and
does not accurately reflect the possible uses.

## The Examples

The examples serve as documentation and showcases what the library and program
can do.
They are actual solutions to actual assignments, with some parts removed.
The generated pdf documents for each example can be found in the Output folder.

## Natural Deduction for LTL and CTL

For the last assignment I came up with a notation and some natural deduction
rules for LTL and CTL.
It is not a rigorous definition and I only dealt with a subset of LTL.
I also introduced two rules that I proved without natural deduction.
All details can be found in Assignment6.pdf.

## Some Issues

The implementation is not very flexible, thus anyone wanting to use these tools
should not deviate very far from the structure of the examples.
The project contains a lot of quick fixes and special cases, which means that
minor things might break the program.
There are some backwards compatibility issues concerning mainly the natural
deduction typesetting, in particular the placement of fresh variables.