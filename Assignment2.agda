module Assignment2 where

open import Logic1
open import Logic2

{-< title >-}
{-- Assignment 2 --}
{-- Henrik Rostedt --}

{-< problem >-}

{-< table to cnf >-}
p₁ : (p q r : 𝔹) → (¬ p ∨ ¬ q ∨ ¬ r) ∧ (¬ p ∨ q ∨ r) ∧ (p ∨ ¬ q ∨ ¬ r) ∧ (p ∨ q ∨ ¬ r) ∧ (p ∨ q ∨ r) !
p₁ ⊤ ⊤ ⊤ = - ⊥
p₁ ⊤ ⊤ ⊥ = - ⊤
p₁ ⊤ ⊥ ⊤ = - ⊤
p₁ ⊤ ⊥ ⊥ = - ⊥
p₁ ⊥ ⊤ ⊤ = - ⊥
p₁ ⊥ ⊤ ⊥ = - ⊤
p₁ ⊥ ⊥ ⊤ = - ⊥
p₁ ⊥ ⊥ ⊥ = - ⊥

{-< problem >-}

{-< subproblem >-}

{-< formula to table >-}
p₂,₁ : (p q : 𝔹) → ø ⇒ ¬ q ⇒ (p ↦ ¬ q) ⇒ ¬ (p ↦ ¬ q) ⇒ p ∧ ¬ (p ↦ ¬ q)
p₂,₁ ⊤ ⊤ = – ⊥ , ⊥ , ⊤ , ⊤
p₂,₁ ⊤ ⊥ = – ⊤ , ⊤ , ⊥ , ⊥
p₂,₁ ⊥ ⊤ = – ⊥ , ⊤ , ⊥ , ⊥
p₂,₁ ⊥ ⊥ = – ⊤ , ⊤ , ⊥ , ⊥

{-< formula to cnf >-}
p₂,₁' : (p q : 𝔹) → p ∧ ¬ (p ↦ ¬ q) ≡ (¬ p ∨ q) ∧ (p ∨ ¬ q) ∧ (p ∨ q)
p₂,₁' ⊤ ⊤ = refl
p₂,₁' ⊤ ⊥ = refl
p₂,₁' ⊥ ⊤ = refl
p₂,₁' ⊥ ⊥ = refl

{-< subproblem >-}

{-< formula to table >-}
p₂,₂ : (p q r : 𝔹) → ø ⇒ ¬ p ⇒ ¬ r ⇒ r ∧ ¬ p ⇒ q ↦ r ∧ ¬ p ⇒ p ∨ (q ↦ r ∧ ¬ p) ⇒ ¬ r ↦ p ∨ (q ↦ r ∧ ¬ p)
p₂,₂ ⊤ ⊤ ⊤ = – ⊥ , ⊥ , ⊥ , ⊥ , ⊤ , ⊤
p₂,₂ ⊤ ⊤ ⊥ = – ⊥ , ⊤ , ⊥ , ⊥ , ⊤ , ⊤
p₂,₂ ⊤ ⊥ ⊤ = – ⊥ , ⊥ , ⊥ , ⊤ , ⊤ , ⊤
p₂,₂ ⊤ ⊥ ⊥ = – ⊥ , ⊤ , ⊥ , ⊤ , ⊤ , ⊤
p₂,₂ ⊥ ⊤ ⊤ = – ⊤ , ⊥ , ⊤ , ⊤ , ⊤ , ⊤
p₂,₂ ⊥ ⊤ ⊥ = – ⊤ , ⊤ , ⊥ , ⊥ , ⊥ , ⊥
p₂,₂ ⊥ ⊥ ⊤ = – ⊤ , ⊥ , ⊤ , ⊤ , ⊤ , ⊤
p₂,₂ ⊥ ⊥ ⊥ = – ⊤ , ⊤ , ⊥ , ⊤ , ⊤ , ⊤

{-< formula to cnf >-}
p₂,₂' : (p q r : 𝔹) → ¬ r ↦ p ∨ (q ↦ r ∧ ¬ p) ≡ p ∨ ¬ q ∨ r
p₂,₂' ⊤ ⊤ ⊤ = refl
p₂,₂' ⊤ ⊤ ⊥ = refl
p₂,₂' ⊤ ⊥ ⊤ = refl
p₂,₂' ⊤ ⊥ ⊥ = refl
p₂,₂' ⊥ ⊤ ⊤ = refl
p₂,₂' ⊥ ⊤ ⊥ = refl
p₂,₂' ⊥ ⊥ ⊤ = refl
p₂,₂' ⊥ ⊥ ⊥ = refl

{-< problem >-}

{-< text >-}
{-- The soundness theorem states the if $\psi_1,…,\psi_n\vdash\varphi$ then $\psi_1,…,\psi_n\vDash\varphi$. --}
{-- Thus if we find a row in the corresponding truth table that doesn't match we can show that $\psi_1,…,\psi_n\nvDash\varphi$ and soundness gives that $\psi_1,…,\psi_n\nvdash\varphi$. --}

{-< subproblem >-}

{-< soundness >-}
p₃,₁ : (p q : 𝔹) → ø ⇒ p ↦ q ⇒ p ∨ q
p₃,₁ ⊥ ⊥ = – ⊤ , ⊥
p₃,₁ p q = etc

{-< equivalence >-}
p₃,₁' : (p q : 𝔹) → p ↦ q ≢ p ∨ q
p₃,₁' ⊥ ⊥ = neql
p₃,₁' p q = meh

{-< subproblem >-}

{-< soundness >-}
p₃,₂ : (p q r : 𝔹) → ø ⇒ q ∨ r ⇒ p ↦ q ⇒ p ↦ r ⇒ p ↦ q ∨ r ⇒ (p ↦ q) ∧ (p ↦ r)
p₃,₂ ⊤ ⊤ ⊥ = – ⊤ , ⊤ , ⊥ , ⊤ , ⊥
p₃,₂ p q r = etc

{-< equivalence >-}
p₃,₂' : (p q r : 𝔹) → p ↦ q ∨ r ≢ (p ↦ q) ∧ (p ↦ r)
p₃,₂' ⊤ ⊤ ⊥ = neql
p₃,₂' p q r = meh

{-< subproblem >-}

{-< text >-}
{-- Here I use ⊤ to represent the empty formula. --}

{-< soundness >-}
p₃,₃ : (p q : 𝔹) → ø ⇒ p ∧ q ⇒ p ↦ q ⇒ ⊤ ⇒ (p ∧ q) ∨ (p ↦ q)
p₃,₃ ⊤ ⊥ = – ⊥ , ⊥ , ⊤ , ⊥
p₃,₃ p q = etc

{-< equivalence >-}
p₃,₃' : (p q : 𝔹) → ⊤ ≢ (p ∧ q) ∨ (p ↦ q)
p₃,₃' ⊤ ⊥ = neql
p₃,₃' p q = meh

{-< problem >-}

{-< subproblem >-}

_⊙_ : 𝔹 → 𝔹 → 𝔹
⊤ ⊙ ⊤ = ⊥
⊤ ⊙ ⊥ = ⊤
⊥ ⊙ b = ⊤ 

{-< formula to table >-}
p₄,₁ : (p q : 𝔹) → ø ⇒ p ∧ q ⇒ ¬ (p ∧ q) ⇒ p ⊙ q
p₄,₁ ⊤ ⊤ = – ⊤ , ⊥ , ⊥
p₄,₁ ⊤ ⊥ = – ⊥ , ⊤ , ⊤
p₄,₁ ⊥ ⊤ = – ⊥ , ⊤ , ⊤
p₄,₁ ⊥ ⊥ = – ⊥ , ⊤ , ⊤

{-< equivalence >-}
p₄,₁' : (p q : 𝔹) → p ⊙ q ≡ ¬ (p ∧ q)
p₄,₁' ⊤ ⊤ = refl
p₄,₁' ⊤ ⊥ = refl
p₄,₁' ⊥ ⊤ = refl
p₄,₁' ⊥ ⊥ = refl

{-< subproblem >-}

{-< text >-}
{-- This can be shown using an inductive argument on the structure of propositional formulas. --}
{-- The base case is the atomic formulas, which can be expressed as they are. --}
{-- The inductive cases are each operation (¬,∧,∨,→). --}
{-- By showing that the truth tables for each operation and proposed replacement formula, each case can be proved. --}
{-- \\\\ --}
{-- The following is a table of suggested replacement formulas: --}

{-< equivalences >-}
{-- eq --}
p₄,₂,₁' : (p : 𝔹) → ¬ p ≡ p ⊙ p
p₄,₂,₁' ⊤ = refl
p₄,₂,₁' ⊥ = refl
{-- eq --}
p₄,₂,₂' : (p q : 𝔹) → p ∧ q ≡ (p ⊙ q) ⊙ (p ⊙ q)
p₄,₂,₂' ⊤ ⊤ = refl
p₄,₂,₂' ⊤ ⊥ = refl
p₄,₂,₂' ⊥ ⊤ = refl
p₄,₂,₂' ⊥ ⊥ = refl
{-- eq --}
p₄,₂,₃' : (p q : 𝔹) → p ∨ q ≡ (p ⊙ p) ⊙ (q ⊙ q)
p₄,₂,₃' ⊤ ⊤ = refl
p₄,₂,₃' ⊤ ⊥ = refl
p₄,₂,₃' ⊥ ⊤ = refl
p₄,₂,₃' ⊥ ⊥ = refl
{-- eq --}
p₄,₂,₄' : (p q : 𝔹) → p ↦ q ≡ p ⊙ (q ⊙ q)
p₄,₂,₄' ⊤ ⊤ = refl
p₄,₂,₄' ⊤ ⊥ = refl
p₄,₂,₄' ⊥ ⊤ = refl
p₄,₂,₄' ⊥ ⊥ = refl

{-< text >-}
{-- Proof that $¬ p ≡ p ⊙ p$: --}

{-< formula to table >-}
p₄,₂,₁ : (p : 𝔹) → ø ⇒ p ⊙ p ⇒ ¬ p
p₄,₂,₁ ⊤ = – ⊥ , ⊥
p₄,₂,₁ ⊥ = – ⊤ , ⊤

{-< text >-}
{-- Proof that $p ∧ q ≡ (p ⊙ q) ⊙ (p ⊙ q)$: --}

{-< formula to table >-}
p₄,₂,₂ : (p q : 𝔹) → ø ⇒ (p ⊙ q) ⇒ (p ⊙ q) ⊙ (p ⊙ q) ⇒ p ∧ q
p₄,₂,₂ ⊤ ⊤ = – ⊥ , ⊤ , ⊤
p₄,₂,₂ ⊤ ⊥ = – ⊤ , ⊥ , ⊥
p₄,₂,₂ ⊥ ⊤ = – ⊤ , ⊥ , ⊥
p₄,₂,₂ ⊥ ⊥ = – ⊤ , ⊥ , ⊥

{-< text >-}
{-- Proof that $p ∨ q ≡ (p ⊙ p) ⊙ (q ⊙ q)$: --}

{-< formula to table >-}
p₄,₂,₃ : (p q : 𝔹) → ø ⇒ (p ⊙ p) ⇒ (q ⊙ q) ⇒ (p ⊙ p) ⊙ (q ⊙ q) ⇒ p ∨ q
p₄,₂,₃ ⊤ ⊤ = – ⊥ , ⊥ , ⊤ , ⊤
p₄,₂,₃ ⊤ ⊥ = – ⊥ , ⊤ , ⊤ , ⊤
p₄,₂,₃ ⊥ ⊤ = – ⊤ , ⊥ , ⊤ , ⊤
p₄,₂,₃ ⊥ ⊥ = – ⊤ , ⊤ , ⊥ , ⊥

{-< text >-}
{-- Proof that $p ↦ q ≡ p ⊙ (q ⊙ q)$: --}

{-< formula to table >-}
p₄,₂,₄ : (p q : 𝔹) → ø ⇒ (q ⊙ q) ⇒ p ⊙ (q ⊙ q) ⇒ p ↦ q
p₄,₂,₄ ⊤ ⊤ = – ⊥ , ⊤ , ⊤
p₄,₂,₄ ⊤ ⊥ = – ⊤ , ⊥ , ⊥
p₄,₂,₄ ⊥ ⊤ = – ⊥ , ⊤ , ⊤
p₄,₂,₄ ⊥ ⊥ = – ⊤ , ⊤ , ⊤
