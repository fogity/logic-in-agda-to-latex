module Main where

import System.Environment
import System.FilePath
import System.Process

import MakeLatex

-- The program creates the LaTeX documnet and runs pdflatex for all supplied Agda files.
main :: IO ()
main = getArgs >>= mapM_ (\path -> makeLatex path >> makePdf path)

-- Runs pdflatex for the given file.
makePdf :: FilePath -> IO ()
makePdf path = do
    callCommand $ "pdflatex " ++ path'
    where
        path' = dropExtension path <.> "tex"