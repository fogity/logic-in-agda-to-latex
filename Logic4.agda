-- This part of the library introduces models and model checking (see Assignmnet4.agda).

module Logic4 where

open import Logic1
open import Logic3

-- The equivalence predicate and natural deduction rules.

postulate
  _≈_ : 𝔹 → 𝔹 → 𝔹

  ≈i : {p : 𝔹} → p ≈ p !
  ≈e : {f : 𝔹 → 𝔹} {p q : 𝔹} → p ≈ q ! → f p ! → f q !

infix 30 _≈_

-- Identity (as in the standard library).

data _≡_ {A : Set} : A → A → Set where
  refl : {a : A} → a ≡ a
  
infix 0 _≡_

cong : {A : Set} {a b : A} (f : A → A) → a ≡ b → f a ≡ f b
cong f refl = refl

-- The following definitions are used to check models.

postulate
  ∇r_ : {A : Set} {a : A} {p q : 𝔹} → p ≡ q → ∇ a · p ≡ q
  ∃r_ : {A : Set} {a : A} {p q : 𝔹} → p ≡ q → ∃ a · p ≡ q
  ∨r : {p q r : 𝔹} → p ≡ r → q ≡ r → p ∨ q ≡ r
  ∧r : {p q r : 𝔹} → p ≡ r → q ≡ r → p ∧ q ≡ r

  _⇒_ : {A : Set} {f : A → 𝔹} {p : 𝔹} (a : A) → f a ≡ p → ((b : A) → f b ≡ p)
  _·_⇛_ : {A : Set} {f : A → A → 𝔹} {p : 𝔹} (a b : A) → f a b ≡ p → ((c d : A) → f c d ≡ p)
  _·_·_⇉_ : {A : Set} {f : A → A → A → 𝔹} {p : 𝔹} (a b c : A) → f a b c ≡ p → ((c d e : A) → f c d e ≡ p)

infix 10 _⇒_
infix 10 _·_⇛_
infix 10 _·_·_⇉_

-- Naturals (as in the standard library)

data ℕ : Set where
  zero : ℕ
  succ : ℕ → ℕ

{-# BUILTIN NATURAL ℕ #-}
