-- This part of the library introduces ways to express truth tables and equivalences (see Assignment2.agda).

module Logic2 where

open import Logic1

-- These definitions are used to build truth tables.

data ø : Set where

data _⇒_ : Set → 𝔹 → Set where
  –_ : (p : 𝔹) → ø ⇒ p
  _,_ : {A : Set} (a : A) (p : 𝔹) → A ⇒ p
  etc : {A : Set} {p : 𝔹} → A ⇒ p

postulate
   - : (p : 𝔹) → p !

infixl -10 _⇒_
infixl -10 _,_

-- These definitions are used to check equivalences of formulas.

data _≡_ : 𝔹 → 𝔹 → Set where
  refl : {p : 𝔹} → p ≡ p

infix 0 _≡_

data _≢_ : 𝔹 → 𝔹 → Set where
  neql : ⊤ ≢ ⊥
  neqr : ⊥ ≢ ⊤
  meh : {p q : 𝔹} → p ≢ q

infix 0 _≢_
