module Assignment6 where

open import Logic1
open import Logic6

{-< title >-}
{-- Assignment 6 --}
{-- Henrik Rostedt --}

-- The following commands where not added to the Haskell code due to backwards compatibility issues.

{-< text >-}
{-- \newcommand{\N}{\mathbb{N}} --}
{-- \newcommand{\M}{\mathcal{M}} --}
{-- \newcommand{\X}{\text{X}} --}
{-- \newcommand{\G}{\text{G}} --}
{-- \newcommand{\F}{\text{F}} --}
{-- \newcommand{\U}{\ \text{U}\ } --}
{-- \newcommand{\E}{\text{E}} --}
{-- \newcommand{\A}{\text{A}} --}

{-< problem >-}

{-< text >-}
{-- Note: This task could have been solved much simpler, but I really liked --}
{-- the idea of extending natural deduction to the LTL logic. --}
{-- \\\\ --}
{-- I will solve this problem by extending and using natural deduction for LTL. --}
{-- In this extension I define the atoms as $p^k$ where $p$ is an --}
{-- atom in LTL and $k∈\N$ is a time step. The value of these atomic formulae is -}
{-- the value of the atom in LTL at time step $k$. --}
{-- In this extension the atom $p$ is equivalent to $p^0$. --}
{-- \\\\ --}
{-- To formulate the rules of this extension I will define $\phi\{l\}$ where --}
{-- $\phi$ is a formula and $l∈\N$ as $\phi$ where the time step for every --}
{-- atom is shifted forward by $l$, e.g. $(p^k↦q^l)\{m\}=p^{k+m}↦q^{l+m}$. --}
{-- Note the following interpretations of some formulae: $\X\phi\{k\}$ is read --}
{-- as $\phi$ is true at $k+1$, $\G\phi\{k\}$ is read as $\phi$ is always true --}
{-- at and after time $k$, and $\F\phi\{k\}$ is read as $\phi$ is true at a time --}
{-- after $k$. --}
{-- \\\\ --}
{-- The introduction and elimintion rules for X and G follow from the the --}
{-- definitions and are as follows: --}
{-- \begin{align*} --}
{-- \frac{\phi\{1\}}{\X\phi}\text{Xi} && --}
{-- \frac{\X\phi}{\phi\{1\}}\text{Xe} && --}
{-- \frac{\boxed{k₀\quad…\phi\{k₀\}}}{\G\phi}\text{Gi} && --}
{-- \frac{\G\phi}{\phi\{k\}}\text{Ge} --}
{-- \end{align*} --}
{-- \\\\ --}
{-- I also have a rule for F (that follows from the definition) that says that --}
{-- if something will eventually happen --}
{-- some time after time $j+k$ then it will also happen some time after time --}
{-- $j$. Lets call the rule FS for Future Shift: --}
{-- \[ \frac{\F\phi\{k\}}{\F\phi}\text{FS} \] --}
{-- \\\\ --}
{-- Finaly I add a rule LR, Loop Reduction, that is defined as --}
{-- \[ \frac{\G(\phi↦\F\phi)\quad\phi}{\G\F\phi}\text{LR} \] --}
{-- which I will prove now: --}
{-- Take any time $k$. We have $\phi$ at time 0 and with $\phi→F\phi$ we have --}
{-- $\phi$ at --}
{-- a time $l>0$. If $l≤k$ we can use $\G(\phi→F\phi)$ until we get an $l'>k$. --}
{-- If $l>k$ (or $l'>k$) we have that $\F\phi$ at time $k$. Thus we have --}
{-- $\G\F\phi$ and --}
{-- we can conclude that the rule LR is indeed correct. --}
{-- \\\\ --}
{-- We can now prove the law by natural deduction: --}

{-< deduction >-}
p2 : (p q : 𝔹) → G(p ^ 0 ↦ X q ^ 0 ) ∧ G(q ^ 0 ↦ F p ^ 0 ) ∧ p ^ 0 ↦ G F p ^ 0 !
p2 p q = ₁₆
  where
  ₁₋₁₅ : G(p ^ 0 ↦ X q ^ 0 ) ∧ G(q ^ 0 ↦ F p ^ 0 ) ∧ p ^ 0 ! → G F p ^ 0 !
  ₁₋₁₅ ₁ = ₁₅
    where
    ₂ : G(p ^ 0 ↦ X q ^ 0 ) !
    ₂ = ∧e₁ ₁
    ₃ : G(q ^ 0 ↦ F p ^ 0 ) ∧ p ^ 0 !
    ₃ = ∧e₂ {G(p ^ 0 ↦ X q ^ 0)} ₁
    ₄ : G(q ^ 0 ↦ F p ^ 0 ) !
    ₄ = ∧e₁ ₃
    ₅ : p ^ 0 !
    ₅ = ∧e₂ {G(q ^ 0 ↦ F p ^ 0)} ₃
    ₆₋₁₃ : (k₀ : ℕ) → p ^ k₀ ↦ F p ^ k₀ !
    ₆₋₁₃ k₀ = ₁₃
      where
      ₆₋₁₂ : p ^ k₀ ! → F p ^ k₀ !
      ₆₋₁₂ ₆ = ₁₂
        where
        ₇ : p ^ k₀ ↦ X q ^ k₀ !
        ₇ = Ge {λ a → p ^ a ↦ X q ^ a} ₂
        ₈ : X q ^ k₀ !
        ₈ = ↦e ₇ ₆
        ₉ : q ^ (k₀ + 1) !
        ₉ = Xe {λ a → q ^ a} ₈
        ₁₀ : q ^ (k₀ + 1) ↦ F p ^ (k₀ + 1) !
        ₁₀ = Ge {λ a → q ^ a ↦ F p ^ a} ₄
        ₁₁ : F p ^ (k₀ + 1) !
        ₁₁ = ↦e ₁₀ ₉
        ₁₂ : F p ^ k₀ !
        ₁₂ = FS {λ a → p ^ a} ₁₁
      ₁₃ : p ^ k₀ ↦ F p ^ k₀ !
      ₁₃ = ↦i ₆₋₁₂
    ₁₄ : G(p ^ 0 ↦ F p ^ 0 ) !
    ₁₄ = Gi ₆₋₁₃
    ₁₅ : G F p ^ 0 !
    ₁₅ = LR ₁₄ ₅
  ₁₆ : G(p ^ 0 ↦ X q ^ 0 ) ∧ G(q ^ 0 ↦ F p ^ 0 ) ∧ p ^ 0 ↦ G F p ^ 0 !
  ₁₆ = ↦i ₁₋₁₅

{-< problem >-}

{-< subproblem >-}

module P3a where

{-< text >-}
{-- The following transition system shows that the formulae are not equal. --}

  data A : Set where
    p : A

  eq : A → A → 𝔹
  eq p p = ⊤

{-< model set >-}
{-- {} --}
  data S : Set where
    s₀ : S
    s₁ : S

{-< model transitions >-}
  _⇒_ : S → S → 𝔹
  s₀ ⇒ s₀ = ⊤
  s₀ ⇒ s₁ = ⊤
  s₁ ⇒ s₀ = ⊤
  _ ⇒ _ = ⊥

{-< model labeling >-}
  L : S → List A
  L(s₀) = p :: []
  L(s₁) = []

{-< text >-}
{-- There exist a path for which every state $p$ is true: --}

{-< model path >-}
  π₀ : List S
  π₀ = s₀ :: s₀ :: []

  val₀ : ⊤ !
  val₀ = val (isG A S L eq (p :: []) π₀)

{-< text >-}
{-- Therefore $s₀\vDash\E\G p$. --}
{-- \\\\ --}
{-- But there also exist a path in which $p$ is not true for every state: --}

{-< model path >-}
  π₁ : List S
  π₁ = s₀ :: s₁ :: []

  val₁ : ⊥ !
  val₁ = val (isG A S L eq (p :: []) π₁)

{-< text >-}
{-- Therefore $s₀\nvDash\A\G p$. --}

{-< subproblem >-}

module P3b where

{-< text >-}
{-- The following transition system shows that the formulae are not equal. --}

  data A : Set where
    p : A
    q : A

  eq : A → A → 𝔹
  eq p p = ⊤
  eq q q = ⊤
  eq _ _ = ⊥

{-< model set >-}
{-- {} --}
  data S : Set where
    s₀ : S
    s₁ : S

{-< model transitions >-}
  _⇒_ : S → S → 𝔹
  s₀ ⇒ s₁ = ⊤
  s₁ ⇒ s₀ = ⊤
  _ ⇒ _ = ⊥

{-< model labeling >-}
  L : S → List A
  L(s₀) = p :: []
  L(s₁) = q :: []

{-< text >-}
{-- There only exist one path from $s₀$: --}

{-< model path >-}
  π : List S
  π = s₀ :: s₁ :: []

  val₀ : ⊤ !
  val₀ = val (isF A S L eq (p :: []) π ∧ isF A S L eq (q :: []) π)

{-< text >-}
{-- In this path eventually $p$ is true --}
{-- and eventually $q$ is true, therefore $s₀\vDash\E\F p ∧ \E\F q$. --}
{-- \\\\ --}
{-- But there is no state in which both $p$ and $q$ is true, --}

  val₁ : ⊥ !
  val₁ = val (isF A S L eq (p :: q :: []) π)

{-< text >-}
{-- therefore $s₀\nvDash\E\F(p∧q)$. --}

{-< subproblem >-}

{-< text >-}
{-- Note: Again, this problem could be solved much simpler, but extending --}
{-- natural deduction to CTL was much more interesting to do. --}
{-- \\\\ --}
{-- These formulae are equivalent and I will prove that using natural deduction --}
{-- extended to CTL. This is an extension to the natural deduction for LTL I --}
{-- defined for Problem 2. First off I will need the introduction and --}
{-- elimination rules for F as well (similar to the rules for $∃$): --}
{-- \begin{align*} --}
{-- \frac{\phi\{k\}}{\F\phi}\text{Fi} && --}
{-- \frac{\F\phi\quad\boxed{k₀\quad\phi\{k₀\}…\chi}}{\chi}\text{Fe} --}
{-- \end{align*} --}
{-- \\\\ --}
{-- The atoms for the CTL extension are $p^k_π$ where $p^k$ is an atom in the --}
{-- LTL extension and $π$ is a path (or branch). Thus $p^k_π$ represents the --}
{-- value of $p$ at time $k$ when following the path $π$. --}
{-- Atoms that are within an A or E do not have a path specified. --}
{-- This extension mixes LTL and CTL formulae, and A and E are viewed as --}
{-- seperate from X, G and F. --}
{-- \\\\ --}
{-- To formulate the rules for A and E I will intorduce $\phi\langleπ\rangle$ --}
{-- as the formula $\phi$ with the path $π$ for every atom. --}
{-- So $(p ↦ \G q)\langleπ\rangle=p_π ↦ \G q_π$. --}
{-- \\\\ --}
{-- Now I can formulate the rules for introduction and elimination of A and E --}
{-- which are similar to the rules for $∃$ and $∀$: --}
{-- \begin{align*} --}
{-- \frac{\boxed{π₀\quad…\phi\langleπ₀\rangle}}{\A\phi}\text{Ai} && --}
{-- \frac{\A\phi}{\phi\langleπ\rangle}\text{Ae} && --}
{-- \frac{\phi\langleπ\rangle}{\E\phi}\text{Ei} && --}
{-- \frac{\E\phi\quad\boxed{π₀\quad\phi\langleπ₀\rangle…\chi}}{\chi}\text{Ee} --}
{-- \end{align*} --}
{-- \\\\ --}
{-- I can now prove the equivalence of the formulae by deriving one form the --}
{-- other and vise versa using this extension to natural deduction: --}

{-< deduction >-}
p2ltr : (p q : 𝔹) → ¬ A G (p ^ 0 * τ ∧ q ^ 0 * τ) ! → E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
p2ltr p q ₁ = ₂₃
  where
  ₂ : E F ¬ p ^ 0 * τ ∨ ¬ E F ¬ p ^ 0 * τ !
  ₂ = LEM {E (F (p ^ zero * τ ↦ ⊥))}
  ₃₋₄ : E F ¬ p ^ 0 * τ ! → E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
  ₃₋₄ ₃ = ₄
    where
    ₄ : E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
    ₄ = ∨i₁ ₃
  ₅₋₂₂ : ¬ E F ¬ p ^ 0 * τ ! → E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
  ₅₋₂₂ ₅ = ₂₂
    where
    ₆₋₂₀ : ¬ E F ¬ q ^ 0 * τ ! → ⊥ !
    ₆₋₂₀ ₆ = ₂₀
      where
      ₇₋₁₈ : (π₀ : ℙ) → G (p ^ 0 * π₀ ∧ q ^ 0 * π₀ ) !
      ₇₋₁₈ π₀ = ₁₈
        where
        ₇₋₁₇ : (k₀ : ℕ) → p ^ k₀ * π₀ ∧ q ^ k₀ * π₀ !
        ₇₋₁₇ k₀ = ₁₇
          where
          ₇₋₁₀ : ¬ p ^ k₀ * π₀ ! → ⊥ !
          ₇₋₁₀ ₇ = ₁₀
            where
            ₈ : F ¬ p ^ 0 * π₀ !
            ₈ = Fi {λ a → ¬ p ^ a * π₀} ₇
            ₉ : E F ¬ p ^ 0 * τ !
            ₉ = Ei {λ a → F ¬ p ^ 0 * a} ₈
            ₁₀ : ⊥ !
            ₁₀ = ¬e ₅ ₉
          ₁₁ : p ^ k₀ * π₀ !
          ₁₁ = PBC ₇₋₁₀
          ₁₂₋₁₅ : ¬ q ^ k₀ * π₀ ! → ⊥ !
          ₁₂₋₁₅ ₁₂ = ₁₅
            where
            ₁₃ : F ¬ q ^ 0 * π₀ !
            ₁₃ = Fi {λ a → ¬ q ^ a * π₀} ₁₂
            ₁₄ : E F ¬ q ^ 0 * τ !
            ₁₄ = Ei {λ a → F ¬ q ^ 0 * a} ₁₃
            ₁₅ : ⊥ !
            ₁₅ = ¬e ₆ ₁₄
          ₁₆ : q ^ k₀ * π₀ !
          ₁₆ = PBC ₁₂₋₁₅
          ₁₇ : p ^ k₀ * π₀ ∧ q ^ k₀ * π₀ !
          ₁₇ = ∧i ₁₁ ₁₆
        ₁₈ : G (p ^ 0 * π₀ ∧ q ^ 0 * π₀ ) !
        ₁₈ = Gi ₇₋₁₇
      ₁₉ : A G (p ^ 0 * τ ∧ q ^ 0 * τ ) !
      ₁₉ = Ai ₇₋₁₈
      ₂₀ : ⊥ !
      ₂₀ = ¬e ₁ ₁₉
    ₂₁ : E F ¬ q ^ 0 * τ !
    ₂₁ = PBC ₆₋₂₀
    ₂₂ : E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
    ₂₂ = ∨i₂ {E (F (p ^ zero * τ ↦ ⊥))} ₂₁
  ₂₃ : E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ !
  ₂₃ = ∨e ₂ ₃₋₄ ₅₋₂₂

{-< deduction >-}
p2rtl : (p q : 𝔹) → E F ¬ p ^ 0 * τ ∨ E F ¬ q ^ 0 * τ ! → ¬ A G (p ^ 0 * τ ∧ q ^ 0 * τ) !
p2rtl p q ₁ = ₂₂
  where
  ₂₋₂₁ : A G (p ^ 0 * τ ∧ q ^ 0 * τ) ! → ⊥ !
  ₂₋₂₁ ₂ = ₂₁
    where
    ₃₋₁₁ : E F ¬ p ^ 0 * τ ! → ⊥ !
    ₃₋₁₁ ₃ = ₁₁
      where
      ₄₋₁₀ : (π₀ : ℙ) → F ¬ p ^ 0 * π₀ ! → ⊥ !
      ₄₋₁₀ π₀ ₄ = ₁₀
        where
        ₅₋₉ : (k₀ : ℕ) → ¬ p ^ k₀ * π₀ ! → ⊥ !
        ₅₋₉ k₀ ₅ = ₉
          where
          ₆ : G (p ^ 0 * π₀ ∧ q ^ 0 * π₀ ) !
          ₆ = Ae {λ a → G (p ^ 0 * a ∧ q ^ 0 * a )} ₂
          ₇ : p ^ k₀ * π₀ ∧ q ^ k₀ * π₀ !
          ₇ = Ge {λ a → p ^ a * π₀ ∧ q ^ a * π₀} ₆
          ₈ : p ^ k₀ * π₀ !
          ₈ = ∧e₁ ₇
          ₉ : ⊥ !
          ₉ = ¬e ₅ ₈
        ₁₀ : ⊥ !
        ₁₀ = Fe ₄ ₅₋₉
      ₁₁ : ⊥ !
      ₁₁ = Ee ₃ ₄₋₁₀
    ₁₂₋₂₀ : E F ¬ q ^ 0 * τ ! → ⊥ !
    ₁₂₋₂₀ ₁₂ = ₂₀
      where
      ₁₃₋₁₉ : (π₀ : ℙ) → F ¬ q ^ 0 * π₀ ! → ⊥ !
      ₁₃₋₁₉ π₀ ₁₃ = ₁₉
        where
        ₁₄₋₁₈ : (k₀ : ℕ) → ¬ q ^ k₀ * π₀ ! → ⊥ !
        ₁₄₋₁₈ k₀ ₁₄ = ₁₈
          where
          ₁₅ : G (p ^ 0 * π₀ ∧ q ^ 0 * π₀ ) !
          ₁₅ = Ae {λ a → G (p ^ 0 * a ∧ q ^ 0 * a )} ₂
          ₁₆ : p ^ k₀ * π₀ ∧ q ^ k₀ * π₀ !
          ₁₆ = Ge {λ a → p ^ a * π₀ ∧ q ^ a * π₀} ₁₅
          ₁₇ : q ^ k₀ * π₀ !
          ₁₇ = ∧e₂ {p ^ k₀ * π₀} ₁₆
          ₁₈ : ⊥ !
          ₁₈ = ¬e ₁₄ ₁₇
        ₁₉ : ⊥ !
        ₁₉ = Fe ₁₃ ₁₄₋₁₈
      ₂₀ : ⊥ !
      ₂₀ = Ee ₁₂ ₁₃₋₁₉
    ₂₁ : ⊥ !
    ₂₁ = ∨e ₁ ₃₋₁₁ ₁₂₋₂₀
  ₂₂ : ¬ A G (p ^ 0 * τ ∧ q ^ 0 * τ) !
  ₂₂ = ¬i ₂₋₂₁

{-< subproblem >-}

module P3d where

{-< text >-}
{-- The following transition system shows that the formulae are not equal. --}

  data A : Set where
    p : A
    q : A
    r : A

  eq : A → A → 𝔹
  eq p p = ⊤
  eq q q = ⊤
  eq r r = ⊤
  eq _ _ = ⊥

{-< model set >-}
{-- {} --}
  data S : Set where
    s₀ : S
    s₁ : S

{-< model transitions >-}
  _⇒_ : S → S → 𝔹
  s₀ ⇒ s₁ = ⊤
  s₁ ⇒ s₁ = ⊤
  _ ⇒ _ = ⊥

{-< model labeling >-}
  L : S → List A
  L(s₀) = p :: []
  L(s₁) = r :: []

{-< text >-}
{-- There only exist one path from $s₀$: --}

{-< model path >-}
  π : List S
  π = s₀ :: s₁ :: s₁ :: []

{-< text >-}
{-- The first state in $\pi$ satisfies $p$, and the second state satisfies --}
{-- $\A[q\U r]$ trivially by satisfying $r$, --}
{-- therefore $s₀\vDash\A[p\U(\A[q\U r])]$. --}
{-- \\\\ --}
{-- But the first state in $\pi$ does not satisfy $\A[p\U q]$ because --}
{-- there is no $q$ in the path, nor does the first state satisfy $r$, --}
{-- therefore $s₀\nvDash\A[(\A[p\U q])\U r]$. --}
