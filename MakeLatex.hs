module MakeLatex (makeLatex) where

import Data.List
import System.FilePath

-- Creates a latex document from annotated Agda code.
makeLatex :: FilePath -> IO ()
makeLatex path = do
    s <- readFile path
    writeFile path' $ convert s
    where
        path' = dropExtension path <.> "tex"

-- Converts Agda code into LaTeX code.
convert :: String -> String
convert = (pre ++) . (++ post) . fix . unlines . concatMap (uncurry convertPart) . splitParts . lines
    where
        pre = unlines
            [ "\\documentclass[a4paper]{article}"
            , "\\usepackage[utf8x]{inputenc}"
            , "\\usepackage[margin=3cm]{geometry}"
            , "\\usepackage{amsmath}"
            , "\\usepackage{amssymb}"
            , "\\usepackage{logicproof}"
            , "\\usepackage{titlesec}"
            , "\\titleformat{\\section}{\\normalfont\\Large\\bfseries}{Problem~\\thesection}{1em}{}"
            , "\\renewcommand{\\thesubsection}{(\\alph{subsection})}"
            , "\\begin{document}"
            ]
        post = "\\end{document}"
        -- Converts some characters that needed to be changed due to Agda limitations or that LaTeX can't handle.
        fix :: String -> String
        fix = replace '↦' '→'
            . replace '·' ' '
            . replace '∇' '∀'
            . replace '≈' '='
            . replace' "Γ" "{\\Gamma}"
            . replace' "π" "{\\pi}"

-- Splits the Agda code into parts based on the annotations.
splitParts :: [String] -> [(String, [String])]
splitParts [] = []
splitParts (x : xs)
    | isMark x = let
        mark = extractMark x
        (ys, zs) = span (not . null) xs
        in (mark, ys) : splitParts zs
    | otherwise = splitParts xs
    where
        isMark :: String -> Bool
        isMark ('{' : '-' : '<' : _) = True
        isMark _ = False
        extractMark :: String -> String
        extractMark = unwords . init . tail . words

-- Converts Agda code based on the annotation.
convertPart :: String -> [String] -> [String]
-- Used to insert a title.
convertPart "title" = makeTitle
-- Used to insert raw LaTeX.
convertPart "text" = map cleanNote
-- Used to force a page break.
convertPart "new page" = const ["\\newpage"]
-- Used to start a new problem section.
convertPart "problem" = const ["\\section{}"]
-- Used to start a new subproblem section.
convertPart "subproblem" = const ["\\subsection{}"]
-- Used to create a formula with descriptions for the logic symbols.
convertPart "english to formula" = convertEnglishToFormula
-- Used to create a natural deduction diagram.
convertPart "deduction" = convertDeduction
-- Used to extract the formula from a truth table definition.
convertPart "table to cnf" = convertTableToCnf
-- Used to extract the truth table from a table definition.
convertPart "formula to table" = convertFormulaToTable
-- Used to extract the second formula from a truth table comparison.
convertPart "formula to cnf" = convertFormulaToCnf
-- Used to extract a row from a truth table.
convertPart "soundness" = convertFormulaToTable . take 2
-- Used to extract the formulas (with equivalence judgement) from a truth table comparison.
convertPart "equivalence" = convertEquivalence
-- As "equivalence" but puts all formulas into a table.
convertPart "equivalences" = convertEquivalences
-- Used to define the set for a model.
convertPart "model set" = convertModelSet
-- Used to define a predicate in a model.
convertPart "model predicate" = convertModelPredicate
-- Used to define a function in a model.
convertPart "model function" = convertModelFunction
-- Used to make a table showing that a sequent holds.
convertPart "model holds" = convertModelHolds
-- Used to make a table showing that a sequent does not hold.
convertPart "model does not hold" = convertModelDoesNotHold
-- Used to define the transitions for a CTL model.
convertPart "model transitions" = convertModelTransitions
-- Used to define the labeling function for a CTL model.
convertPart "model labeling" = convertModelLabeling
-- Used to define a path within a CTL model.
convertPart "model path" = convertModelPath
convertPart x = error $ "unknown mark: " ++ x

makeTitle :: [String] -> [String]
makeTitle [x, y] =
    [ "\\title{" ++ cleanNote x ++ "}"
    , "\\author{" ++ cleanNote y ++ "}"
    , "\\date{}"
    , "\\maketitle"
    ]
makeTitle _ = error "malformed title"

convertEnglishToFormula :: [String] -> [String]
convertEnglishToFormula (_ : x : xs)
    | null body = [formula]
    | otherwise = formula : pre ++ body ++ post
    where
        formula = ("\\[" ++) . (++ "\\]") . tail . dropWhile (/= '=') $ x
        pre = 
            [ "\\begin{center}"
            , "\\begin{tabular}{rl}"
            ]
        body = mapInit (++ "\\\\") . map formatRow $ xs
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            ]
        formatRow :: String -> String
        formatRow s = '$' : p ++ "$ &" ++ tail n
            where
                (p, n) = span (/= '=') $ cleanNote s

-- Definition used for tagging lines in a natural deduction.
data Tagged = Assumption String | Fresh String Int | FreshAssumption String | Step String String | EndBox | EndLine

convertDeduction :: [String] -> [String]
convertDeduction (x : _ : w : xs) = pre ++ body ++ post
    where
        pre =
            [ "{"
            , if hasFresh tagged then "\\setlength\\subproofhorizspace{2em}" else ""
            , "\\begin{logicproof}{" ++ show (countDepth 0 0 tagged) ++ "}"
            ]
        body = premises ++ steps
        premises = map (++ "& premise \\\\") . extractPremises . tail . dropWhile (/= '→') $ x
        steps = concatMap untag . endLines . adjustFresh $ tagged
        post =
            [ "\\end{logicproof}"
            , "}"
            ]
        tagged = tag (numSpaces w) . zip (map numSpaces xs) $ xs
        fix :: String -> String
        fix = replace' "X" "\\X"
            . replace' "G" "\\G"
            . replace' "F" "\\F"
            . replace' "A" "\\A"
            . replace' "E" "\\E"
            . fixSuper
        fixSuper :: String -> String
        fixSuper ('*' : ' ' : 'τ' : more) = fixSuper more
        fixSuper ('*' : ' ' : '(' : more) = '_' : '{' : some ++ '}' : fixSuper (tail more')
            where (some, more') = span (/= ')') more
        fixSuper ('*' : ' ' : more) = '_' : '{' : some ++ '}' : fixSuper more'
            where (some, more') = span (/= ' ') more
        fixSuper ('^' : ' ' : '0' : more) = fixSuper more
        fixSuper ('^' : ' ' : '(' : more) = '^' : '{' : some ++ '}' : fixSuper (tail more')
            where (some, more') = span (/= ')') more
        fixSuper ('^' : ' ' : more) = '^' : '{' : some ++ '}' : fixSuper more'
            where (some, more') = span (/= ' ') more
        fixSuper "" = ""
        fixSuper (c : more) = c : fixSuper more
        hasFresh :: [Tagged] -> Bool
        hasFresh [] = False
        hasFresh (Fresh _ _ : _) = True
        hasFresh (FreshAssumption _ : _) = True
        hasFresh (_ : ts) = hasFresh ts
        countDepth :: Int -> Int -> [Tagged] -> Int
        countDepth _ m [] = m
        countDepth n m (t : ts) = case t of
            Assumption _ -> countDepth (n + 1) (max (n + 1) m) ts
            Fresh _ _ -> countDepth (n + 1) (max (n + 1) m) ts
            FreshAssumption _ -> countDepth (n + 1) (max (n + 1) m) ts            
            EndBox -> countDepth (n - 1) m ts
            _ -> countDepth n m ts
        tag :: Int -> [(Int, String)] -> [Tagged]
        tag _ [] = []
        tag _ [_] = []
        tag n ((m, s) : ys)
            | m < n
            = EndBox : tag m ((m, s) : ys)
            | '𝔹' `elem` s || 'ℕ' `elem` s || 'ℙ' `elem` s
            , numArg > 1
            = FreshAssumption s : tag (n + 2) (tail ys')
            | '𝔹' `elem` s || 'ℕ' `elem` s || 'ℙ' `elem` s
            = Fresh s 0 : tag (n + 2) (tail ys')
            | numArg > 0
            = Assumption s : tag (n + 2) (tail ys')
            | otherwise
            = Step s t : tag n ys'
            where
                ((_, t) : ys') = ys
                numArg = count '→' s
        adjustFresh :: [Tagged] -> [Tagged]
        adjustFresh (Fresh s1 n1 : Fresh s2 n2 : ts) = Fresh s1 (n1 + 1) : Fresh s2 n2 : adjustFresh ts
        adjustFresh (t : ts) = t : adjustFresh ts
        adjustFresh [] = []
        endLines :: [Tagged] -> [Tagged]
        endLines [t] = [t]
        endLines (t : EndBox : ts) = t : EndBox : endLines ts
        endLines (Fresh s n : ts) = Fresh s n : endLines ts
        endLines (t : ts) = t : EndLine : endLines ts
        untag :: Tagged -> [String]
        untag (Assumption s) = "\\begin{subproof}" : map (++ "& assumption") assumptions
            where
                assumptions = extractPremises . tail . dropWhile (/= ':') $ s
        untag (Fresh s n) = ["\\begin{subproof}", "\\llap{$" ++ fresh ++ space ++ "$\\quad}"]
            where
                space = concat $ replicate (n + 1) "\\qquad"
                fresh = takeWhile (/= ':') . tail . dropWhile (/= '(') $ s
        untag (FreshAssumption s) = "\\begin{subproof}" : ("\\llap{$" ++ fresh ++ "$\\quad}") : map (++ "& assumption") assumptions
            where
                fresh = takeWhile (/= ':') . tail . dropWhile (/= '(') $ s
                assumptions = extractPremises . tail . dropWhile (/= '→') $ s
        untag (Step s t) = [extractFormula s ++ "&" ++ extractStep t]
        untag EndBox = ["\\end{subproof}"]
        untag EndLine = ["\\\\"]
        numSpaces :: String -> Int
        numSpaces (' ' : s) = 1 + numSpaces s
        numSpaces _ = 0
        extractPremises :: String -> [String]
        extractPremises s
            | "!" <- r = []
            | otherwise = fix p : ps
            where
                (p, r) = span (/= '!') s
                ps = extractPremises . tail . dropWhile (/= '→') $ r
        extractFormula :: String -> String
        extractFormula = fix . takeWhile (/= '!') . tail . dropWhile (/= ':')
        extractStep :: String -> String
        extractStep
            = unwords . mapCenter (++ ",") . mapTail numSubToNorm . words
            . dropBraces . tail . dropWhile (/= '=')
        dropBraces :: String -> String
        dropBraces s
            | null r = s
            | otherwise = l ++ r'
            where
                r' = (tail . dropWhile (/= '}')) r
                (l, r) = span (/= '{') s

convertTableToCnf :: [String] -> [String]
convertTableToCnf (x : _) = ["\\[" ++ format x ++ "\\]"]
    where
        format :: String -> String
        format = takeWhile (/= '!') . tail . dropWhile (/= '→')

convertFormulaToTable :: [String] -> [String]
convertFormulaToTable (x : xs) = pre ++ header : sep : body ++ post
    where
        vars = words . takeWhile (/= ':') . tail . dropWhile (/= '(') $ x
        forms = splitForms . tail . dropWhile (/= '⇒') $ x
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{" ++ replicate (length vars) 'c' ++ concat (replicate (length forms) "|c") ++ "}"
            ]
        header = intercalate "&" (map (\s -> "$" ++ s ++ "$") $ vars ++ forms) ++ "\\\\"
        sep = "\\hline"
        body = mapInit (++ "\\\\") . map formatRow $ xs
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            ]
        splitForms :: String -> [String]
        splitForms s
            | null r = [f]
            | otherwise = f : fs
            where
                (f, r) = span (/= '⇒') s
                fs = splitForms $ tail r
        formatRow :: String -> String
        formatRow = intercalate "&" . map formatBool . filter (isBool) . words
            where
                isBool :: String -> Bool
                isBool "⊤" = True
                isBool "⊥" = True
                isBool _ = False
                formatBool :: String -> String
                formatBool "⊤" = "T"
                formatBool "⊥" = "F"

convertFormulaToCnf :: [String] -> [String]
convertFormulaToCnf (x : _) = ["\\[" ++ format x ++ "\\]"]
    where
        format :: String -> String
        format = tail . dropWhile (/= '≡')

convertEquivalence :: [String] -> [String]
convertEquivalence (x : _) = ["\\[" ++ format x ++ "\\]"]
    where
        format :: String -> String
        format = tail . dropWhile (/= '→')

convertEquivalences :: [String] -> [String]
convertEquivalences xs = pre ++ body ++ post
    where
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{r|r}"
            , "$\\varphi$&$\\varphi'$\\\\"
            , "\\hline"
            ]
        body = mapInit (++ "\\\\") . map formatEq . splitEqs . tail $ xs
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            ]
        splitEqs :: [String] -> [String]
        splitEqs as
            | null cs = [b]
            | otherwise = b : bs
            where
                (b : _, cs) = span (/= "{-- eq --}") as
                bs = splitEqs $ tail cs
        formatEq :: String -> String
        formatEq s = '$' : t ++ "$&$" ++ tail r ++ "$"
            where
                (t, r) = span (/= '≡') . tail . dropWhile (/= '→') $ s

convertModelSet :: [String] -> [String]
convertModelSet (m : x : xs) = ["\\[" ++ formatSet x ++ concat (mapTail (',':) (map formatElem xs)) ++ "\\}\\]"]
    where
        formatSet :: String -> String
        formatSet s = takeWhile (/= ':') (replace' "data" "" s) ++ "^" ++ cleanNote m ++ "=\\{"
        formatElem :: String -> String
        formatElem s = numSubToNorm (takeWhile (/= ':') s)

convertModelPredicate :: [String] -> [String]
convertModelPredicate (m : x : xs) = ["\\[" ++ formatSet x ++ concat (mapTail (',':) (map formatElem (init xs))) ++ "\\}\\]"]
    where
        formatSet :: String -> String
        formatSet s = takeWhile (/= ':') s ++ "^" ++ cleanNote m ++ "=\\{"
        formatElem :: String -> String
        formatElem s = unwords $ map numSubToNorm zs
            where
                ys = words s
                zs = takeWhile (/= "=") $ tail ys
                
convertModelFunction :: [String] -> [String]
convertModelFunction (m : x : xs) = pre ++ map formatRow xs ++ post
    where
        m' = "^" ++ cleanNote m
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{l}"
            , ("$" ++) . (++ "$") . unwords . mapOdd (++ m') $ words x 
            ]
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            ]
        formatRow :: String -> String
        formatRow = ("\\\\" ++) . ("$" ++) . (++ "$") . unwords . mapHead (++ m') . map numSubToNorm . words

convertModelHolds :: [String] -> [String]
convertModelHolds (m : x : xs) = pre ++ body ++ post
    where
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{" ++ columns ++ "}"
            , h
            , "\\hline"
            ]
        (h, f) = header n x
        n = count 'A' x
        columns = concat . mapTail ('|':) $ replicate (n + 1) "l"
        body = mapInit (++ "\\\\") . map formatRow $ xs
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            , "\\[" ++ cleanNote m ++ "\\vDash" ++ f ++ "\\]"
            ]
        header :: Int -> String -> (String, String)
        header 0 s = ('$': f++ "$\\\\", f)
            where
                f = takeWhile (/= '≡') . tail . dropWhile (/= '→') $ s
        header c s = ('$' : v ++ '$' : '&' : z, f)
            where
                (z, f) = header (c - 1) r
                s' = dropWhile (== '(') $ dropWhile (/= '(') s
                (v, r) = span (/= ':') s'
        formatRow :: String -> String
        formatRow s = concat . mapTail ('&':) . map numSubToNorm $ ys ++ [note]
            where
                ends = ["⇒", "⇛", "⇉", "∇r", "∃r", "refl", "cong"]
                junk = ["·", "="]
                ys = tail . filter (not . (`elem` junk)) . takeWhile (not . (`elem` ends)) . words $ s
                s' = dropWhile (/= '{') s
                note = if null s' then "holds" else cleanNote s'

convertModelDoesNotHold :: [String] -> [String]
convertModelDoesNotHold (m : x : xs) = pre ++ body ++ post
    where
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{" ++ columns ++ "}"
            , h
            , "\\hline"
            ]
        (h, f) = header n x
        n = count 'A' x
        columns = concat . mapTail ('|':) $ replicate (n + 1) "l"
        body = mapInit (++ "\\\\") . map formatRow $ xs
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            , "\\[" ++ cleanNote m ++ "\\nvDash" ++ f ++ "\\]"
            ]
        header :: Int -> String -> (String, String)
        header 0 s = ('$': f++ "$\\\\", f)
            where
                f = takeWhile (/= '≡') . tail . dropWhile (/= '→') $ s
        header c s = ('$' : v ++ '$' : '&' : z, f)
            where
                (z, f) = header (c - 1) r
                s' = dropWhile (== '(') $ dropWhile (/= '(') s
                (v, r) = span (/= ':') s'
        formatRow :: String -> String
        formatRow s = concat . mapTail ('&':) . map numSubToNorm $ ys ++ [note]
            where
                ends = ["⇒", "⇛", "⇉", "∇r", "∃r", "refl", "cong"]
                junk = ["·", "="]
                ys = tail . filter (not . (`elem` junk)) . takeWhile (not . (`elem` ends)) . words $ s
                s' = dropWhile (/= '{') s
                note = if null s' then "does not hold" else cleanNote s'

convertModelTransitions :: [String] -> [String]
convertModelTransitions (_ : xs) = ["\\[" ++ concat (mapTail (',' :) $ map format $ init xs) ++ "\\]"]
    where
        format :: String -> String
        format x | (a : _ : b : _) <- words x = a ++ "→" ++ b
        
convertModelLabeling :: [String] -> [String]
convertModelLabeling (_ : xs) = pre ++ mapInit (++ "\\\\") (map formatRow xs) ++ post
    where
        pre =
            [ "\\begin{center}"
            , "\\begin{tabular}{l}"
            ]
        post =
            [ "\\end{tabular}"
            , "\\end{center}"
            ]
        formatRow :: String -> String
        formatRow x = '$' : a ++ "=\\{" ++ b' ++ "\\}$"
            where
                (a, b) = span (/= '=') x
                b' = unwords . mapTail (',':) . filter (/= "::") . init . words . tail $ b

convertModelPath :: [String] -> [String]
convertModelPath (_ : x : _)
    | (p : _ : xs) <- words x
    , ys <- replace "[]" "…" . replace "::" "→" $ xs
    = ["\\[" ++ p ++ "=" ++ unwords ys ++ "\\]"]

-- Extracts the body of a block comment.
cleanNote :: String -> String
cleanNote = unwords . init . tail . words 

-- The following are some generic utility functions.

replace :: Eq a => a -> a -> [a] -> [a]
replace _ _ [] = []
replace x y (z : zs) = (if x == z then y else z) : replace x y zs

replace' :: String -> String -> String -> String
replace' _ _ [] = []
replace' f r s
    | Just s' <- delPrefix f s = r ++ replace' f r s'
    | (c : s') <- s = c : replace' f r s'
    where
        delPrefix :: String -> String -> Maybe String
        delPrefix [] s = Just s
        delPrefix (c1 : s1) (c2 : s2)
            | c1 == c2 = delPrefix s1 s2
            | otherwise = Nothing

mapInit :: (a -> a) -> [a] -> [a]
mapInit _ [] = []
mapInit _ [x] = [x]
mapInit f (x : xs) = f x : mapInit f xs

mapTail :: (a -> a) -> [a] -> [a]
mapTail _ [] = []
mapTail f (x : xs) = x : map f xs

mapCenter :: (a -> a) -> [a] -> [a]
mapCenter _ [] = []
mapCenter f (x : xs) = x : mapInit f xs

mapSnd :: (a -> b) -> [(c, a)] -> [(c, b)]
mapSnd _ [] = []
mapSnd f ((c, x) : xs) = (c, f x) : mapSnd f xs

mapOdd :: (a -> a) -> [a] -> [a]
mapOdd f [] = []
mapOdd f (x : xs) = f x : mapEven f xs

mapEven :: (a -> a) -> [a] -> [a]
mapEven f [] = []
mapEven f (x : xs) = x : mapOdd f xs

mapHead :: (a -> a) -> [a] -> [a]
mapHead f [] = []
mapHead f (x : xs) = f x : xs

numSubToNorm :: String -> String
numSubToNorm [] = []
numSubToNorm (c : s)
    | Just c' <- subToNorm c = c' : numSubToNorm s
    | otherwise = c : s
    where
        subToNorm :: Char -> Maybe Char
        subToNorm '₁' = Just '1'
        subToNorm '₂' = Just '2'
        subToNorm '₃' = Just '3'
        subToNorm '₄' = Just '4'
        subToNorm '₅' = Just '5'
        subToNorm '₆' = Just '6'
        subToNorm '₇' = Just '7'
        subToNorm '₈' = Just '8'
        subToNorm '₉' = Just '9'
        subToNorm '₀' = Just '0'
        subToNorm '₋' = Just '-'
        subToNorm ' ' = Just ' '
        subToNorm _ = Nothing

count :: Eq a => a -> [a] -> Int
count a = count' 0
    where
        count' n [] = n
        count' n (x : xs)
            | x == a = count' (n + 1) xs
            | otherwise = count' n xs