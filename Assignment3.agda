module Assignment3 where

open import Logic1
open import Logic3

{-< title >-}
{-- Assignment 3 --}
{-- Henrik Rostedt --}

-- The following command definition was added due to bad backwards compatibility.

{-< text >-}
{-- \newcommand{\F}{F} --}

{-< problem >-}

module P1 where

  postulate
    S : 𝔹 → 𝔹
    L : 𝔹 → 𝔹
    A : 𝔹 ,, 𝔹 → 𝔹

{-< subproblem >-}

{-< english to formula >-}
  p₁,₁ : 𝔹 → 𝔹 → 𝔹
  p₁,₁ x y = ∃ x · (S(x) ∧ ∇ y · (L(y) ↦ ¬ A(x , y)))

{-< subproblem >-}

{-< english to formula >-}
  p₁,₂ : 𝔹 → 𝔹 → 𝔹
  p₁,₂ x y = ∇ y · (L(y) ↦ ∃ x · (S(x) ∧ ¬ A(x , y)))

{-< problem >-}

module P2 where

  postulate
    P : 𝔹 → 𝔹
    Q : 𝔹 → 𝔹
    F : 𝔹 → 𝔹
    R : 𝔹 ,, 𝔹 → 𝔹
    S : 𝔹

{-< subproblem >-}

{-< deduction >-}
  p₂,₁ : (x y : 𝔹) → ∇ x · ∇ y · R(x , y) ! → ∇ x · R(x , x) !
  p₂,₁ x y ₁ = ₄
    where
    ₂₋₃ : (x₀ : 𝔹) → R(x₀ , x₀) !
    ₂₋₃ x₀ = ₃
      where
      ₂ : ∇ y · R(x₀ , y) !
      ₂ = ∇e {x₀} ₁
      ₃ : R(x₀ , x₀) !
      ₃ = ∇e {x₀} ₂
    ₄ : ∇ x · R(x , x) !
    ₄ = ∇i ₂₋₃

{-< subproblem >-}

{-< deduction >-}
  p₂,₂ : (x : 𝔹) → ∇ x · (P(x) ↦ S) ! → ∃ x · P(x) ↦ S !
  p₂,₂ x ₁ = ₇
    where
    ₂₋₆ : ∃ x · P(x) ! → S !
    ₂₋₆ ₂ = ₆
      where
      ₃₋₅ : (x₀ : 𝔹) → P(x₀) ! → S !
      ₃₋₅ x₀ ₃ = ₅
        where
        ₄ : P(x₀) ↦ S !
        ₄ = ∇e {x₀} ₁
        ₅ : S !
        ₅ = ↦e ₄ ₃
      ₆ : S !
      ₆ = ∃e ₂ ₃₋₅
    ₇ : ∃ x · P(x) ↦ S !
    ₇ = ↦i ₂₋₆

{-< subproblem >-}

{-< deduction >-}
  p₂,₃ : (x y : 𝔹) → ∃ x · ∇ y · R(x , y) ! → ∇ y · ∃ x · R(x , y) !
  p₂,₃ x y ₁ = ₆
    where
    ₂₋₅ : (x₀ : 𝔹) → ∇ y · R(x₀ , y) ! → ∇ y · ∃ x · R(x , y) !
    ₂₋₅ x₀ ₂ = ₅
      where
      ₃₋₄ : (y₀ : 𝔹) → ∃ x · R(x , y₀) !
      ₃₋₄ y₀ = ₄
        where
        ₃ : R(x₀ , y₀) !
        ₃ = ∇e {y₀} ₂
        ₄ : ∃ x · R(x , y₀) !
        ₄ = ∃i {x₀} ₃
      ₅ : ∇ y · ∃ x · R(x , y) !
      ₅ = ∇i ₃₋₄
    ₆ : ∇ y · ∃ x · R(x , y) !
    ₆ = ∃e ₁ ₂₋₅

{-< subproblem >-}

{-< deduction >-}
  p₂,₄ : (x : 𝔹) → ∃ x · (P(x) ∧ Q(x)) ! → ∇ x · (Q(x) ↦ F(x)) ! → ∃ x · (P(x) ∧ F(x)) !
  p₂,₄ x ₁ ₂ = ₁₀
    where
    ₃₋₉ : (x₀ : 𝔹) → P(x₀) ∧ Q(x₀) ! → ∃ x · (P(x) ∧ F(x)) !
    ₃₋₉ x₀ ₃ = ₉
      where
      ₄ : P(x₀) !
      ₄ = ∧e₁ ₃
      ₅ : Q(x₀) !
      ₅ = ∧e₂ {P(x₀)} ₃
      ₆ : Q(x₀) ↦ F(x₀) !
      ₆ = ∇e {x₀} ₂
      ₇ : F(x₀) !
      ₇ = ↦e ₆ ₅
      ₈ : P(x₀) ∧ F(x₀) !
      ₈ = ∧i ₄ ₇
      ₉ : ∃ x · (P(x) ∧ F(x)) !
      ₉ = ∃i {x₀} ₈
    ₁₀ : ∃ x · (P(x) ∧ F(x)) !
    ₁₀ = ∃e ₁ ₃₋₉

{-< subproblem >-}

{-< deduction >-}
  p₂,₅ : (x t : 𝔹) → S ↦ ∃ x · P(x) ! → ∃ x · (S ↦ P(x)) !
  p₂,₅ x t ₁ = ₁₇
    where
    ₂ : S ∨ ¬ S !
    ₂ = LEM {S}
    ₃₋₁₀ : S ! → ∃ x · (S ↦ P(x)) !
    ₃₋₁₀ ₃ = ₁₀
      where
      ₄ : ∃ x · P(x) !
      ₄ = ↦e ₁ ₃
      ₅₋₉ : (x₀ : 𝔹) → P(x₀) ! → ∃ x · (S ↦ P(x)) !
      ₅₋₉ x₀ ₅ = ₉
        where
        ₆₋₇ : S ! → P(x₀) !
        ₆₋₇ ₆ = ₇
          where
          ₇ : P(x₀) !
          ₇ = copy ₅
        ₈ : S ↦ P(x₀) !
        ₈ = ↦i ₆₋₇
        ₉ : ∃ x · (S ↦ P(x)) !
        ₉ = ∃i {x₀} ₈
      ₁₀ : ∃ x · (S ↦ P(x)) !
      ₁₀ = ∃e ₄ ₅₋₉
    ₁₁₋₁₆ : ¬ S ! → ∃ x · (S ↦ P(x)) !
    ₁₁₋₁₆ ₁₁ = ₁₆
      where
      ₁₂₋₁₄ : S ! → P(t) !
      ₁₂₋₁₄ ₁₂ = ₁₄
        where
        ₁₃ : ⊥ !
        ₁₃ = ¬e ₁₁ ₁₂
        ₁₄ : P(t) !
        ₁₄ = ⊥e ₁₃
      ₁₅ : S ↦ P(t) !
      ₁₅ = ↦i ₁₂₋₁₄
      ₁₆ : ∃ x · (S ↦ P(x)) !
      ₁₆ = ∃i {t} ₁₅
    ₁₇ : ∃ x · (S ↦ P(x)) !
    ₁₇ = ∨e ₂ ₃₋₁₀ ₁₁₋₁₆

{-< new page >-}

{-< problem >-}

module P3 where

  postulate
    D : 𝔹 → 𝔹

{-< text >-}
{-- Note that the $x₀$ on line 9 belongs to the box 9-13. --}

{-< deduction >-}
  p₃ : (x y t : 𝔹) → ∃ x · (D(x) ↦ ∇ y · D(y)) !
  p₃ x y t = ₂₅
    where
    ₁ : ∇ y · D(y) ∨ ¬ ∇ y · D(y) !
    ₁ = LEM {∇ y · D(y)}
    ₂₋₆ : ∇ y · D(y) ! →  ∃ x · (D(x) ↦ ∇ y · D(y)) !
    ₂₋₆ ₂ = ₆
      where
      ₃₋₄ : D(t) ! → ∇ y · D(y) !
      ₃₋₄ ₃ = ₄
        where
        ₄ : ∇ y · D(y) !
        ₄ = copy ₂
      ₅ : D(t) ↦ ∇ y · D(y) !
      ₅ = ↦i ₃₋₄
      ₆ : ∃ x · (D(x) ↦ ∇ y · D(y)) !
      ₆ = ∃i {t} ₅
    ₇₋₂₄ : ¬ ∇ y · D(y) ! →  ∃ x · (D(x) ↦ ∇ y · D(y)) !
    ₇₋₂₄ ₇ = ₂₄
      where
      ₈₋₁₄ : ¬ ∃ x · ¬ D(x) ! → ∇ y · D(y) !
      ₈₋₁₄ ₈ = ₁₄
        where
        ₉₋₁₃ : (x₀ : 𝔹) → D(x₀) !
        ₉₋₁₃ x₀ = ₁₃
          where
          ₉₋₁₂ : ¬ D(x₀) ! → ⊥ !
          ₉₋₁₂ ₉ = ₁₁
            where
            ₁₀ : ∃ x · ¬ D(x) !
            ₁₀ = ∃i {x₀} ₉
            ₁₁ : ⊥ !
            ₁₁ = ¬e ₈ ₁₀
          ₁₂ : ¬ (¬ D(x₀)) !
          ₁₂ = ¬i ₉₋₁₂
          ₁₃ : D(x₀) !
          ₁₃ = ¬¬e ₁₂
        ₁₄ : ∇ y · D(y) !
        ₁₄ = ∇i ₉₋₁₃
      ₁₅ : ¬ ∃ x · ¬ D(x) ↦ ∇ y · D(y) !
      ₁₅ = ↦i ₈₋₁₄
      ₁₆ : ¬ (¬ ∃ x · ¬ D(x)) !
      ₁₆ = MP {¬ ∃ x · ¬ D(x)} ₁₅ ₇
      ₁₇ : ∃ x · ¬ D(x) !
      ₁₇ = ¬¬e ₁₆
      ₁₈₋₂₃ : (x₀ : 𝔹) → ¬ D(x₀) ! → ∃ x · (D(x) ↦ ∇ y · D(y)) !
      ₁₈₋₂₃ x₀ ₁₈ = ₂₃
        where
        ₁₉₋₂₁ : D(x₀) ! → ∇ y · D(y) !
        ₁₉₋₂₁ ₁₉ = ₂₁
          where
          ₂₀ : ⊥ !
          ₂₀ = ¬e ₁₈ ₁₉
          ₂₁ : ∇ y · D(y) !
          ₂₁ = ⊥e ₂₀
        ₂₂ : D(x₀) ↦ ∇ y · D(y) !
        ₂₂ = ↦i ₁₉₋₂₁
        ₂₃ : ∃ x · (D(x) ↦ ∇ y · D(y)) !
        ₂₃ = ∃i {x₀} ₂₂
      ₂₄ : ∃ x · (D(x) ↦ ∇ y · D(y)) !
      ₂₄ = ∃e ₁₇ ₁₈₋₂₃
    ₂₅ : ∃ x · (D(x) ↦ ∇ y · D(y)) !
    ₂₅ = ∨e ₁ ₂₋₆ ₇₋₂₄
