module Assignment1 where

open import Logic1

{-< title >-}
{-- Assignment 1 --}
{-- Henrik Rostedt --}

{-< problem >-}

{-< subproblem >-}

{-< english to formula >-}
p₁,₁ : 𝔹 → 𝔹 → 𝔹 → 𝔹
p₁,₁ p q r = p ↦ q ∧ r
{-- p = The sun shines --}
{-- q = Emmy eats ice cream --}
{-- r = Kurt eats ice cream --}

{-< subproblem >-}

{-< english to formula >-}
p₁,₂ : 𝔹 → 𝔹 → 𝔹 → 𝔹
p₁,₂ p q r = (¬ p ∧ q ∧ r) ∨ (p ∧ ¬ q ∧ r) ∨ (p ∧ q ∧ ¬ r)
{-- p = Ada likes cats --}
{-- q = Haskell likes cats --}
{-- r = Bertrand likes cats --}

{-< problem >-}

{-< subproblem >-}

{-< deduction >-}
p₂,₁ : (p q r : 𝔹) → p ∧ (q ∧ r) ! → (p ∧ q) ∧ r !
p₂,₁ p q r ₁ = ₇
  where
  ₂ : p !
  ₂ = ∧e₁ ₁
  ₃ : (q ∧ r) !
  ₃ = ∧e₂ {p} ₁
  ₄ : r !
  ₄ = ∧e₂ {q} ₃
  ₅ : q !
  ₅ = ∧e₁ ₃
  ₆ : p ∧ q !
  ₆ = ∧i {p} ₂ ₅
  ₇ : (p ∧ q) ∧ r !
  ₇ = ∧i {p ∧ q} ₆ ₄

{-< subproblem >-}

{-< deduction >-}
p₂,₂ : (p q : 𝔹) → p ↦ q ↦ p !
p₂,₂ p q = ₅
  where
  ₁₋₄ : p ! → q ↦ p !
  ₁₋₄ ₁ = ₄
    where
    ₂₋₃ : q ! → p !
    ₂₋₃ ₂ = ₃
      where
      ₃ : p !
      ₃ = copy ₁
    ₄ : q ↦ p !
    ₄ = ↦i ₂₋₃
  ₅ : p ↦ q ↦ p !
  ₅ = ↦i ₁₋₄

{-< subproblem >-}

{-< deduction >-}
p₂,₃ : (p q r : 𝔹) → (p ↦ r) ∨ (q ↦ r) ! → p ∧ q ↦ r !
p₂,₃ p q r ₁ = ₁₀
  where
  ₂₋₉ : p ∧ q ! → r !
  ₂₋₉ ₂ = ₉
    where
    ₃ : p !
    ₃ = ∧e₁ ₂
    ₄₋₅ : p ↦ r ! → r !
    ₄₋₅ ₄ = ₅
      where
      ₅ : r !
      ₅ = ↦e ₄ ₃
    ₆ : q !
    ₆ = ∧e₂ {p} ₂
    ₇₋₈ : q ↦ r ! → r !
    ₇₋₈ ₇ = ₈
      where
      ₈ : r !
      ₈ = ↦e ₇ ₆
    ₉ : r !
    ₉ = ∨e ₁ ₄₋₅ ₇₋₈
  ₁₀ : p ∧ q ↦ r !
  ₁₀ = ↦i ₂₋₉

{-< subproblem >-}

{-< deduction >-}
p₂,₄ : (p : 𝔹) → p ↦ ¬ p ! → ¬ p ↦ p ! → ⊥ !
p₂,₄ p ₁ ₂ = ₈
  where
  ₃₋₅ : p ! → ⊥ !
  ₃₋₅ ₃ = ₅
    where
    ₄ : ¬ p !
    ₄ = ↦e ₁ ₃
    ₅ : ⊥ !
    ₅ = ¬e ₄ ₃
  ₆ : ¬ p !
  ₆ = ¬i ₃₋₅
  ₇ : p !
  ₇ = ↦e ₂ ₆
  ₈ : ⊥ !
  ₈ = ¬e ₆ ₇

{-< subproblem >-}

{-< deduction >-}
p₂,₅ : (p q : 𝔹) → ¬ (p ∧ q) ! → ¬ p ∨ ¬ q !
p₂,₅ p q ₁ = ₁₁
  where
  ₂ : p ∨ ¬ p !
  ₂ = LEM {p}
  ₃₋₈ : p ! → ¬ p ∨ ¬ q !
  ₃₋₈ ₃ = ₈
    where
    ₄₋₆ : q ! → ⊥ !
    ₄₋₆ ₄ = ₆
      where
      ₅ : p ∧ q !
      ₅ = ∧i ₃ ₄
      ₆ : ⊥ !
      ₆ = ¬e ₁ ₅
    ₇ : ¬ q !
    ₇ = ¬i ₄₋₆
    ₈ : ¬ p ∨ ¬ q !
    ₈ = ∨i₂ {¬ p} ₇
  ₉₋₁₀ : ¬ p ! → ¬ p ∨ ¬ q !
  ₉₋₁₀ ₉ = ₁₀
    where
    ₁₀ : ¬ p ∨ ¬ q !
    ₁₀ = ∨i₁ ₉
  ₁₁ : ¬ p ∨ ¬ q !
  ₁₁ = ∨e ₂ ₃₋₈ ₉₋₁₀

{-< problem >-}

{-< deduction >-}
p₃ : (p q : 𝔹) → (p ↦ q) ↦ p ! → p !
p₃ p q ₁ = ₂₀
  where
  ₂ : q ∨ ¬ q !
  ₂ = LEM {q}
  ₃₋₁₀ : q ! → p !
  ₃₋₁₀ ₃ = ₁₀
    where
    ₄₋₉ : ¬ p ! → ⊥ !
    ₄₋₉ ₄ = ₉
      where
      ₅ : ¬ (p ↦ q) !
      ₅ = MP {p ↦ q} ₁ ₄
      ₆₋₇ : p ! → q !
      ₆₋₇ ₆ = ₇
        where
        ₇ : q !
        ₇ = ₃
      ₈ : p ↦ q !
      ₈ = ↦i ₆₋₇
      ₉ : ⊥ !
      ₉ = ¬e ₅ ₈
    ₁₀ : p !
    ₁₀ = PBC ₄₋₉
  ₁₁₋₁₉ : ¬ q ! → p !
  ₁₁₋₁₉ ₁₁ = ₁₉
    where
    ₁₂₋₁₈ : ¬ p ! → ⊥ !
    ₁₂₋₁₈ ₁₂ = ₁₈
      where
      ₁₃ : ¬ (p ↦ q) !
      ₁₃ = MP {p ↦ q} ₁ ₁₂
      ₁₄₋₁₆ : p ! → q !
      ₁₄₋₁₆ ₁₄ = ₁₆
        where
        ₁₅ : ⊥ !
        ₁₅ = ¬e ₁₂ ₁₄
        ₁₆ : q !
        ₁₆ = ⊥e ₁₅
      ₁₇ : p ↦ q !
      ₁₇ = ↦i ₁₄₋₁₆
      ₁₈ : ⊥ !
      ₁₈ = ¬e ₁₃ ₁₇
    ₁₉ : p !
    ₁₉ = PBC ₁₂₋₁₈
  ₂₀ : p !
  ₂₀ = ∨e ₂ ₃₋₁₀ ₁₁₋₁₉
