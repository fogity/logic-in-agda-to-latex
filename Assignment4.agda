module Assignment4 where

open import Logic1
open import Logic3
open import Logic4

{-< title >-}
{-- Assignment 4 --}
{-- Henrik Rostedt --}

{-< problem >-}

module P1 where

  postulate
    f : 𝔹 → 𝔹
    g : 𝔹 → 𝔹

{-< subproblem >-}

{-< deduction >-}
  p₁,₁ : (c b x : 𝔹) → ∇ x · f(f(x)) ≈ f(x) ! → f(b) ≈ c ! → c ≈ f(c) !
  p₁,₁ c b x ₁ ₂ = ₆
    where
    ₃ : f(f(b)) ≈ f(b) !
    ₃ = ∇e {b} ₁
    ₄ : f(c) ≈ c !
    ₄ = ≈e ₂ ₃
    ₅ : f(c) ≈ f(c) !
    ₅ = ≈i
    ₆ : c ≈ f(c) !
    ₆ = ≈e {λ a → a ≈ f(c)} ₄ ₅

{-< subproblem >-}

{-< deduction >-}
  p₁,₂ : (x y : 𝔹) → ∇ x · ∇ y · (x ≈ g(y) ↦ f(x) ≈ y) ! → ∇ x · f(g(x)) ≈ x !
  p₁,₂ x y ₁ = ₁₀
    where
    ₂₋₉ : (y₀ : 𝔹) → f(g(y₀)) ≈ y₀ !
    ₂₋₉ y₀ = ₉
      where
      ₂ : g(y₀) ≈ g(y₀) !
      ₂ = ≈i
      ₃ : ∃ x · (x ≈ g(y₀)) !
      ₃ = ∃i {g(y₀)} ₂
      ₄₋₈ : (x₀ : 𝔹) → x₀ ≈ g(y₀) ! → f(g(y₀)) ≈ y₀ !
      ₄₋₈ x₀ ₄ = ₈
        where
        ₅ : ∇ y · (x₀ ≈ g(y) ↦ f(x₀) ≈ y) !
        ₅ = ∇e {x₀} ₁
        ₆ : x₀ ≈ g(y₀) ↦ f(x₀) ≈ y₀ !
        ₆ = ∇e {y₀} ₅
        ₇ : f(x₀) ≈ y₀ !
        ₇ = ↦e ₆ ₄
        ₈ : f(g(y₀)) ≈ y₀ !
        ₈ = ≈e ₄ ₇
      ₉ : f(g(y₀)) ≈ y₀ !
      ₉ = ∃e ₃ ₄₋₈
    ₁₀ : ∇ x · f(g(x)) ≈ x !
    ₁₀ = ∇i ₂₋₉

{-< problem >-}

{-< subproblem >-}

module P3a where

{-< text >-}
{-- The sequent is invalid, this is shown by the following model $\mathcal{M}$: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  R : A ,, A → 𝔹
  R ( ₀ , ₀ ) = ⊤
  R ( ₁ , ₁ ) = ⊤
  R _ = ⊥

{-< text >-}
{-- We can show that for all $x$ there is a $y$ such that the premise holds: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l : (x : A) → ((y : A) → ∇ x · ∃ y · R(x , y) ≡ ⊤)
  l ₀ = ₀ ⇒ ∇r ∃r refl
  l ₁ = ₁ ⇒ ∇r ∃r refl

{-< text >-}
{-- But for all $y$ there is an $x$ such that the conclusion does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r : (y : A) (x : A) → ∃ y · ∇ x · R(x , y) ≡ ⊥
  r ₀ = ₁ ⇒ ∃r ∇r refl
  r ₁ = ₀ ⇒ ∃r ∇r refl

{-< text >-}
{-- Thus the sequent is not valid. --}

{-< subproblem >-}

module P3b where

{-< text >-}
{-- The sequent is invalid, this is shown by the following model $\mathcal{M}$: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  S : A → 𝔹
  S ₀ = ⊤
  S _ = ⊥

{-< model predicate >-}
{-- \mathcal{M} --}
  T : A → 𝔹
  T ₁ = ⊤
  T _ = ⊥

{-< text >-}
{-- We can show that for all $x$ the premise holds: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l : (x : A) → ∇ x · (S(x) ∨ T(x)) ≡ ⊤
  l ₀ = ∇r refl
  l ₁ = ∇r refl

{-< text >-}
{-- To differentiate the two for all cases the $x$s has been numbered. --}
{-- We can show that there are a pair of $x₁, x₂$ such that the conclusion does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r : (x₁ : A) (x₂ : A) → ∇ x₁ · S(x₁) ∨ ∇ x₂ · T(x₂) ≡ ⊥
  r = ₁ · ₀ ⇛ ∨r (∇r refl) (∇r refl)

{-< text >-}
{-- Thus the sequent is not valid. --}

{-< subproblem >-}

module P3c where

{-< text >-}
{-- The sequent is valid, this is shown by natural deduction: --}

  postulate
    R : 𝔹 → 𝔹 → 𝔹
    S : 𝔹 → 𝔹
    T : 𝔹 → 𝔹

{-< deduction >-}
  p : (x y : 𝔹) → ∇ x · ∃ y · (S(x) ↦ T(y)) ! → ∇ x · (S(x) ↦ ∃ y · T(y)) !
  p x y ₁ = ₉
    where
    ₂₋₈ : (x₀ : 𝔹) → S(x₀) ↦ ∃ y · T(y) !
    ₂₋₈ x₀ = ₈
      where
      ₂₋₇ : S(x₀) ! → ∃ y · T(y) !
      ₂₋₇ ₂ = ₇
        where
        ₃ : ∃ y · (S(x₀) ↦ T(y)) !
        ₃ = ∇e {x₀} ₁
        ₄₋₆ : (y₀ : 𝔹) → S(x₀) ↦ T(y₀) ! → ∃ y · T(y) !
        ₄₋₆ y₀ ₄ = ₆
          where
          ₅ : T(y₀) !
          ₅ = ↦e ₄ ₂
          ₆ : ∃ y · T(y) !
          ₆ = ∃i {y₀} ₅
        ₇ : ∃ y · T(y) !
        ₇ = ∃e ₃ ₄₋₆
      ₈ : S(x₀) ↦ ∃ y · T(y) !
      ₈ = ↦i ₂₋₇
    ₉ : ∇ x · (S(x) ↦ ∃ y · T(y)) !
    ₉ = ∇i ₂₋₈

{-< subproblem >-}

module P3d where

{-< text >-}
{-- The sequent is invalid, this is shown by the following model $\mathcal{M}$: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A
    ₂ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  R : A ,, A → 𝔹
  R ( ₀ , ₀ ) = ⊤
  R ( ₁ , ₁ ) = ⊤
  R ( ₂ , ₂ ) = ⊤
  R ( ₀ , ₁ ) = ⊤
  R ( ₁ , ₀ ) = ⊤
  R ( ₀ , ₂ ) = ⊤
  R ( ₂ , ₀ ) = ⊤
  R _ = ⊥

{-< text >-}
{-- We can show that for all $x,y$ the premises holds: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l₁ : (x : A) → ∇ x · R(x , x) ≡ ⊤
  l₁ ₀ = ∇r refl
  l₁ ₁ = ∇r refl
  l₁ ₂ = ∇r refl

{-< model holds >-}
{-- \mathcal{M} --}
  l₂ : (x : A) (y : A) → ∇ x · ∇ y · (R(x , y) ↦ R(y , x)) ≡ ⊤
  l₂ ₀ ₀ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₀ ₁ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₀ ₂ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₁ ₀ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₁ ₁ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₁ ₂ = ∇r ∇r refl {-- holds (premise does not hold) --}
  l₂ ₂ ₀ = ∇r ∇r refl {-- premise and conclusion holds --}
  l₂ ₂ ₁ = ∇r ∇r refl {-- holds (premise does not hold) --}
  l₂ ₂ ₂ = ∇r ∇r refl {-- premise and conclusion holds --}

{-< text >-}
{-- But there is an $x, y$ and $z$ such that the conclusion does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r : (x : A) (y : A) (z : A) → ∇ x · ∇ y · ∇ z · (R(x , y) ∧ R(y , z) ↦ R(x , z)) ≡ ⊥
  r = ₁ · ₀ · ₂ ⇉ ∇r ∇r ∇r refl {-- the premise holds but not the conclusion --}

{-< text >-}
{-- Thus the sequent is not valid. --}

{-< problem >-}

{-< subproblem >-}

module P4a where

  postulate
    Q : 𝔹 → 𝔹
    h : 𝔹 → 𝔹

{-< deduction >-}
  p : (x y t : 𝔹) → ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
  p x y t = ₈
    where
    ₁ : Q(h(t)) ∨ ¬ Q(h(t)) !
    ₁ = LEM {Q(h(t))}
    ₂₋₄ : Q(h(t)) ! → ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
    ₂₋₄ ₂ = ₄
      where
      ₃ : Q(h(t)) ∨ ¬ Q(h(h(t))) !
      ₃ = ∨i₁ ₂
      ₄ : ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
      ₄ = ∃i {h(t)} ₃
    ₅₋₇ : ¬ Q(h(t)) ! → ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
    ₅₋₇ ₅ = ₇
      where
      ₆ : Q(t) ∨ ¬ Q(h(t)) !
      ₆ = ∨i₂ {Q(t)} ₅
      ₇ : ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
      ₇ = ∃i {t} ₆
    ₈ : ∃ x · (Q(x) ∨ ¬ Q(h(x))) !
    ₈ = ∨e ₁ ₂₋₄ ₅₋₇

{-< subproblem >-}

module P4b where

{-< text >-}
{-- We will use the soundness theorem to show that the sequent is invalid. --}
{-- To do this we define a model $\mathcal{M}$: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  Q : A → 𝔹
  Q ₀ = ⊤
  Q _ = ⊥

{-< model function >-}
{-- \mathcal{M} --}
  h : A → A
  h ( ₀ ) = ₁
  h ( ₁ ) = ₀

{-< text >-}
{-- From the definition of $h$ we can deduce that any term in the model --}
{-- can be reduced to either 0 or 1. We can then show that for all terms --}
{-- takinig the value of 1 neither term of the sequent is valid: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r₁ : (t : A) → Q(t) ≡ ⊥
  r₁ = ₁ ⇒ refl

{-< model does not hold >-}
{-- \mathcal{M} --}
  r₂ : (t : A) → ¬ Q(h(t)) ≡ ⊥
  r₂ = ₁ ⇒ refl

{-< text >-}
{-- Thus $\mathcal{M}\nvDash Q(t)∨¬Q(h(t))$. --}
{-- Then the soundness theorem shows $\nvdash Q(t)∨¬Q(h(t))$. --}
