-- This part of the library introduces LTL (a subset) and CTL (see Assignment6.agda).

module Logic6 where

open import Logic1

-- Naturals (as in the standard library).

data ℕ : Set where
  zero : ℕ
  succ : ℕ → ℕ

_+_ : ℕ → ℕ → ℕ
zero + n = n
succ m + n = succ (m + n)

{-# BUILTIN NATURAL ℕ #-}

-- This definition represents paths, where tau represents no particular path.

data ℙ : Set where
  τ : ℙ

-- Lists and functions on lists.

data List (A : Set) : Set where
  [] : List A
  _::_ : A → List A → List A

infixr 20 _::_

map : {A B : Set} → (A → B) → List A → List B
map f [] = []
map f (x :: xs) = f x :: map f xs

zip : {A B C : Set} → (A → B → C) → List A → List B → List C
zip f [] ys = []
zip f (x :: xs) [] = []
zip f (x :: xs) (y :: ys) = f x y :: zip f xs ys

all : List 𝔹 → 𝔹
all [] = ⊤
all (⊤ :: xs) = all xs
all (⊥ :: xs) = ⊥

any : List 𝔹 → 𝔹
any [] = ⊥
any (⊤ :: xs) = ⊤
any (⊥ :: xs) = any xs

-- Determines if an elements is contained in a list based on an equality operation.

_∈_<_> : {A : Set} → A → List A → (A → A → 𝔹) → 𝔹
a ∈ xs < eq > = any (map (eq a) xs)

postulate
  -- Used to validate if an expression is true or if it is false.

  val : (p : 𝔹) → p ! 

  -- Used to annotate a logic symbol with a time stamp.

  _^_ : 𝔹 → ℕ → 𝔹

  -- Connectives for LTL.

  X_ : 𝔹 → 𝔹
  G_ : 𝔹 → 𝔹
  F_ : 𝔹 → 𝔹

  -- Used to annotate a logic symbol with a path.

  _*_ : 𝔹 → ℙ → 𝔹
  
  -- Connectives for CTL.

  A_ : 𝔹 → 𝔹
  E_ : 𝔹 → 𝔹
  
  -- Natural deduction rules for LTL and CTL.

  Xi : {f : ℕ → 𝔹} {i : ℕ} → f (i + 1) ! → X f i !
  Xe : {f : ℕ → 𝔹} {i : ℕ} → X f i ! → f (i + 1) !

  Gi : {f : ℕ → 𝔹} → ((i : ℕ) → f i !) → G f 0 !
  Ge : {f : ℕ → 𝔹} {i : ℕ} → G f 0 ! → f i !

  Fi : {f : ℕ → 𝔹} {i : ℕ} → f i ! → F f 0 !
  Fe : {f : ℕ → 𝔹} {p : 𝔹} → F f 0 ! → ((i : ℕ) → f i ! → p !) → p !

  Ai : {f : ℙ → 𝔹} → ((i : ℙ) → f i !) → A f τ !
  Ae : {f : ℙ → 𝔹} {i : ℙ} → A f τ ! → f i !

  Ei : {f : ℙ → 𝔹} {i : ℙ} → f i ! → E f τ !
  Ee : {f : ℙ → 𝔹} {p : 𝔹} → E f τ ! → ((i : ℙ) → f i ! → p !) → p !

  FS : {f : ℕ → 𝔹} {i j : ℕ} → F f (i + j) ! → F f i !
  LR : {p : 𝔹} → G(p ↦ F p) ! → p ! → G F p !

infixl 20 _^_
infixl 20 _*_

infixl 15 X_
infixl 15 G_
infixl 15 F_

infixl 15 A_
infixl 15 E_

-- Predicates to determine if something holds globaly or eventually.

isG : (A S : Set) → (S → List A) → (A → A → 𝔹) → List A → List S → 𝔹
isG A S L eq as xs = all (map (λ s → all (map (λ a → a ∈ L s < eq >) as)) xs)

isF : (A S : Set) → (S → List A) → (A → A → 𝔹) → List A → List S → 𝔹
isF A S L eq as xs = any (map (λ s → all (map (λ a → a ∈ L s < eq >) as)) xs)
