-- This part of the library introduces predicate logic.

module Logic3 where

open import Logic1

-- Used to express pairs of values.

data _,,_ : Set → Set → Set where
  _,_ : {A : Set} → A → A → A ,, A

-- The predicate connectives and natural deduction rules. Nabla used instead of ∀ do to Agda restriction.

postulate
  ∇_·_ : {A : Set} → A → 𝔹 → 𝔹
  ∃_·_ : {A : Set} → A → 𝔹 → 𝔹
  
  ∇e : {t x : 𝔹} {f : 𝔹 → 𝔹} → ∇ x · f x ! → f t !
  ∇i : {f : 𝔹 → 𝔹} {x : 𝔹} → ((x₀ : 𝔹) → f x₀ !) → ∇ x · f x !
  ∃e : {x p : 𝔹} {f : 𝔹 → 𝔹} → ∃ x · f x ! → ((x₀ : 𝔹) → f x₀ ! → p !) → p !
  ∃i : {t x : 𝔹} {f : 𝔹 → 𝔹} → f t ! → ∃ x · f x !
