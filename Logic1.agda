-- This part of the library introduces connectives and natural deducation for propositional logic.

module Logic1 where

-- Basic logic values and connectives.

data 𝔹 : Set where
  ⊤ : 𝔹
  ⊥ : 𝔹

_∧_ : 𝔹 → 𝔹 → 𝔹
⊤ ∧ b = b
⊥ ∧ b = ⊥

infixr 10 _∧_

_∨_ : 𝔹 → 𝔹 → 𝔹
⊤ ∨ b = ⊤
⊥ ∨ b = b

infixr 10 _∨_

_↦_ : 𝔹 → 𝔹 → 𝔹
⊤ ↦ b = b
⊥ ↦ b = ⊤

infixr 5 _↦_

¬_ : 𝔹 → 𝔹
¬_ = _↦ ⊥

infixl 15 ¬_

-- Data type to encode logic formulas as types.

data _! : 𝔹 → Set where

infix 0 _!

-- The natural deduction rules, including excluded middle rules.

postulate
  copy : {p : 𝔹} → p ! → p !

  ∧i : {p q : 𝔹} → p ! → q ! → p ∧ q !
  ∧e₁ : {p q : 𝔹} → p ∧ q ! → p !
  ∧e₂ : {p q : 𝔹} → p ∧ q ! → q !
  ∨i₁ : {p q : 𝔹} → p ! → p ∨ q !
  ∨i₂ : {p q : 𝔹} → q ! → p ∨ q !
  ∨e : {p q r : 𝔹} → p ∨ q ! → (p ! → r !) → (q ! → r !) → r !
  ↦i : {p q : 𝔹} → (p ! → q !) → p ↦ q !
  ↦e : {p q : 𝔹} → p ↦ q ! → p ! → q !
  ¬i : {p : 𝔹} → (p ! → ⊥ !) → ¬ p !
  ¬e : {p : 𝔹} → ¬ p ! → p ! → ⊥ !
  ⊥e : {p : 𝔹} → ⊥ ! → p !
  ¬¬e : {p : 𝔹} → ¬ (¬ p) ! → p !
  
  MP : {p q : 𝔹} → p ↦ q ! → ¬ q ! → ¬ p !
  ¬¬i : {p : 𝔹} → p ! → ¬ (¬ p) !
  PBC : {p : 𝔹} → (¬ p ! → ⊥ !) → p !
  LEM : {p : 𝔹} → p ∨ ¬ p !
