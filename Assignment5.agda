module Assignment5 where

open import Logic1
open import Logic3
open import Logic4

{-< title >-}
{-- Assignment 5 --}
{-- Henrik Rostedt --}

{-< problem >-}

module P2 where

{-< text >-}
{-- With the model $\mathcal{M}$ defined as: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  R : A ,, A → 𝔹
  R ( ₀ , ₀ ) = ⊤
  R ( ₁ , ₀ ) = ⊤
  R _ = ⊥

{-< model function >-}
{-- \mathcal{M} --}
  s : A → A
  s ( ₀ ) = ₀
  s ( ₁ ) = ₀

{-< text >-}
{-- The left part of $\varphi$ holds: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l : (x : A) → ∇ x · R(x , s(x)) ≡ ⊤
  l ₀ = ∇r refl
  l ₁ = ∇r refl

{-< text >-}
{-- And the right part of $\varphi$ holds with $y=1$: --}

{-< model holds >-}
{-- \mathcal{M} --}
  r : (x : A) → (y : A) → ∃ y · ¬ ∃ x · R(x , y) ≡ ⊤
  r ₀ = ₁ ⇒ ∃r cong ¬_ (∃r refl)
  r ₁ = ₁ ⇒ ∃r cong ¬_ (∃r refl)

{-< text >-}
{-- Therefore: \[ \mathcal{M} \vDash \varphi \] --}

module P2' where

{-< text >-}
{-- Then with the model $\mathcal{M'}$ defined as: --}

{-< model set >-}
{-- \mathcal{M'} --}
  data A : Set where
    ₀ : A

{-< model predicate >-}
{-- \mathcal{M'} --}
  R : A ,, A → 𝔹
  R _ = ⊥

{-< model function >-}
{-- \mathcal{M'} --}
  s : A → A
  s ( ₀ ) = ₀

{-< text >-}
{-- The left part of $\varphi$ does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M'} --}
  l : (x : A) → ∇ x · R(x , s(x)) ≡ ⊥
  l ₀ = ∇r refl

{-< text >-}
{-- Therefore: \[ \mathcal{M'} \nvDash \varphi \] --}

{-< problem >-}

{-< subproblem >-}

module P3a where

{-< text >-}
{-- With the model $\mathcal{M}$ defined as: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  P : A → 𝔹
  P ₀ = ⊤
  P _ = ⊥

{-< text >-}
{-- The premise holds with $x=1$ as a witness: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l : (x : A) → ¬ ∇ x · P(x) ≡ ⊤
  l = ₁ ⇒ cong ¬_ (∇r refl) {-- witness --}

{-< text >-}
{-- But the conclusion does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r : (x : A) → ∇ x · ¬ P(x) ≡ ⊥
  r = ₀ ⇒ ∇r refl

{-< text >-}
{-- Thus the sequent is not valid, due to soundness. --}

{-< subproblem >-}

module P3b where

{-< text >-}
{-- With the model $\mathcal{M}$ defined as: --}

{-< model set >-}
{-- \mathcal{M} --}
  data A : Set where
    ₀ : A
    ₁ : A

{-< model predicate >-}
{-- \mathcal{M} --}
  P : A → 𝔹
  P ₀ = ⊤
  P _ = ⊥

{-< model predicate >-}
{-- \mathcal{M} --}
  Q : A → 𝔹
  Q _ = ⊥

{-< text >-}
{-- The first premise holds with $x=1$ as a witness: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l₁ : (x : A) → ∃ x · (P(x) ↦ Q(x)) ≡ ⊤
  l₁ = ₁ ⇒ ∃r refl {-- witness --}

{-< text >-}
{-- The second premise holds with $x=0$ as a witness: --}

{-< model holds >-}
{-- \mathcal{M} --}
  l₂ : (x : A) → ∃ x · P(x) ≡ ⊤
  l₂ = ₀ ⇒ ∃r refl {-- witness --}

{-< text >-}
{-- But the conclusion does not hold: --}

{-< model does not hold >-}
{-- \mathcal{M} --}
  r : (x : A) → ∃ x · Q(x) ≡ ⊥
  r ₀ = ∃r refl
  r ₁ = ∃r refl

{-< text >-}
{-- Thus the sequent is not valid, due to soundness. --}
